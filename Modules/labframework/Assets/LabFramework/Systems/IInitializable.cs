﻿namespace lab.framework
{
    public interface IInitializable: ISystem
    {
        void Initialize(params dynamic[] arg);
    }
}