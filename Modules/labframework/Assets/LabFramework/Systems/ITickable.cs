﻿namespace lab.framework
{
    public interface ITickable: ISystem
    {
        void Tick();
    }
}