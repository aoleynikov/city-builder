namespace lab.framework
{
    public interface ILateInitializable: ISystem
    {
        void LateInitialize();
    }
}