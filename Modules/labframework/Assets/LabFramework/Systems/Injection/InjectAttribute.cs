using System;

namespace lab.framework
{
    [AttributeUsage(AttributeTargets.All)]
    public class InjectAttribute : Attribute
    {
    }
}