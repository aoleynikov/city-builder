using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace lab.framework
{
    public class DIResolver
    {
        private readonly Dictionary<Type, object> registeredInstances = new Dictionary<Type, object>();

        public void AddTypeToInstance(Type type, object instance)
        {
            registeredInstances.Add(type, instance);
        }
        
        public void ResolveAll()
        {
            foreach (var instance in registeredInstances)
            {
                FieldInjection(instance);
                PropertyInjection(instance);
                MethodInjection(instance);
            }
        }

        private void FieldInjection(KeyValuePair<Type, object> instance)
        {
            var allFields = instance.Value.GetType().GetRuntimeFields();
            var injectedFields = allFields.Where(x => x.IsDefined(typeof(InjectAttribute)));

            foreach (var prop in injectedFields)
            {
                Type typeToInject = prop.FieldType;

                foreach (var injectedInstance in registeredInstances)
                {
                    if (typeToInject == injectedInstance.Key)
                    {
                        prop.SetValue(instance.Value, injectedInstance.Value);
                    }
                    
                }
            }
        }
        
        private void PropertyInjection(KeyValuePair<Type, object> instance)
        {
            var allProperties = instance.Key.GetRuntimeProperties();
            var injectedProperties = allProperties.Where(x => x.IsDefined(typeof(InjectAttribute)));

            foreach (var prop in injectedProperties)
            {
                Type typeToInject = prop.PropertyType;

                foreach (var injectedInstance in registeredInstances)
                {
                    if (typeToInject == injectedInstance.Key)
                    {
                        prop.SetValue(instance.Value, injectedInstance.Value);
                    }
                    
                }
            }
        }

        private void MethodInjection(KeyValuePair<Type, object> instance)
        {
            var allMethods = instance.Value.GetType().GetMethods();
            var injectedMethods = allMethods.Where(x => x.IsDefined(typeof(InjectAttribute)));

            List<object> args = new List<object>();
            
            foreach (var injectedMethod in injectedMethods)
            {
                ParameterInfo[] parameterInfo = injectedMethod.GetParameters();

                foreach (var paramInfo in parameterInfo)
                {
                    Type typeToInject = paramInfo.ParameterType;
                   
                    foreach (var registeredInstance in registeredInstances)
                    {
                        if (typeToInject == registeredInstance.Key)
                        {
                            args.Add(registeredInstance.Value);
                        }
                    }
                }
                
                injectedMethod.Invoke(instance.Value, args.ToArray());
            }
        }
    }
}