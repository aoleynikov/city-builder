using UnityEngine;

namespace lab.framework
{
    public class BootstrapBase: MonoBehaviour
    {
        protected SystemsContainer systemsContainer;
        
        public virtual void Awake()
        {
            systemsContainer = new SystemsContainer();
        }
        
        protected virtual void Update()
        {
            systemsContainer.UpdateAll();
        }

        protected virtual void LateUpdate()
        {
            systemsContainer.LateUpdateAll();
        }

        protected virtual void OnDestroy()
        {
            systemsContainer.DisposeAll();
        }
        
        protected AsyncProcessor CreateAsyncProcessor(string processorName)
        {
            GameObject asyncGO = new GameObject(processorName);
            AsyncProcessor asyncProcessor = asyncGO.AddComponent<AsyncProcessor>();
            return asyncProcessor;
        }
    }
}