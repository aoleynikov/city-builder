using UnityEditor;
using UnityEditor.Callbacks;
using UnityEngine;

namespace lab.snippets
{
    [InitializeOnLoad]
    public class VersionIncrementor
    {
        [PostProcessBuild(1)]
        public static void OnPostprocessBuild(BuildTarget target, string pathToBuiltProject)
        {
            Debug.Log("Build v" + PlayerSettings.bundleVersion + " (" + PlayerSettings.Android.bundleVersionCode + ")");
            IncreaseBuild();
        }
 
        static void IncrementVersion(int majorIncr, int minorIncr, int buildIncr)
        {
            string[] lines = PlayerSettings.bundleVersion.Split('.');
 
            int MajorVersion = int.Parse(lines[0]) + majorIncr;
            int MinorVersion = int.Parse(lines[1]) + minorIncr;
 
            PlayerSettings.bundleVersion = MajorVersion.ToString("0") + "." +
                                           MinorVersion.ToString("0");

            Debug.Log("<color=magenta>Version: " + PlayerSettings.bundleVersion + "</color>");
        }
 
        [MenuItem("Lab/Build/Increase Minor Version")]
        private static void IncreaseMinor()
        {
            IncrementVersion(0, 1, 0);
        }
 
        [MenuItem("Lab/Build/Increase Major Version")]
        private static void IncreaseMajor()
        {
            IncrementVersion(1, 0, 0);
        }
 
        private static void IncreaseBuild()
        {
            IncrementVersion(0, 0, 1);
        }
    }
}