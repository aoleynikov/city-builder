using UnityEngine;

namespace ao.CityBuilder
{
    [CreateAssetMenu(fileName = "PlacementSettings", menuName = "AO/PlacementSettings")]
    public class PlacementSettings : ScriptableObject
    {
        public int AreaDimension = 12;
        public float TileSize = 10;

        public GameObject InvisibleTilePrefab;
    }
}