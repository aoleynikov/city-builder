using lab.framework;
using UnityEngine;

namespace ao.CityBuilder
{
    [CreateAssetMenu(fileName = "InitSettings", menuName = "AO/InitSettings")]
    public class InitSettings : ScriptableObject
    {
        [Tooltip("Building settings")]
        public BuildingsSettings BuildingsSettings;
        
        [Tooltip("UI settings")]
        public UISettings UISettings;
        
        [Tooltip("Resource settings")]
        public ResourceSettings ResourceSettings;
        
        [Tooltip("Placement settings")]
        public PlacementSettings PlacementSettings;
        
        [HideInInspector] public AsyncProcessor AsyncProcessor;
    }
}