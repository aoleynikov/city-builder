using UnityEngine;

namespace ao.CityBuilder
{
    [CreateAssetMenu(fileName = "UISettings", menuName = "AO/UISettings")]
    public class UISettings : ScriptableObject
    {
        public UIBuildingItem UIBuildingItem;
    }
}