using UnityEngine;

namespace ao.CityBuilder
{
    [CreateAssetMenu(fileName = "ResourceSettings", menuName = "AO/ResourceSettings")]
    public class ResourceSettings : ScriptableObject
    {
        [Tooltip("Initial resources that user has")]
        public PlayerResources InitialResources;

        public ResourceProducerView ResourceProducerView;
    }
}