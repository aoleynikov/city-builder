using System.Collections.Generic;
using UnityEngine;

namespace ao.CityBuilder
{
    [CreateAssetMenu(fileName = "BuildingsSettings", menuName = "AO/BuildingsSettings")]
    public class BuildingsSettings : ScriptableObject
    {
        public List<Building> Buildings = new List<Building>();
    }
}