using System.Collections.Generic;

namespace ao.CityBuilder
{
    public enum LayerType
    {
        Buildings,
        Area
    }
    
    public static class Layers
    {
        public static Dictionary<LayerType, int> LayerMask = new Dictionary<LayerType, int>
        {
            {LayerType.Buildings, 1 << 30},
            {LayerType.Area, 1 << 31}
        };
    }
}