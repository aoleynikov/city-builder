namespace ao.CityBuilder
{
    public enum BuildingType
    {
        Building2x2,
        Building3x2,
        Building3x3,
        Bench1x1,
        Tree2x2
    }
}