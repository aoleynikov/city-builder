namespace ao.CityBuilder
{
    public enum FitStatus
    {
        Fits,
        Occupied,
        OutOfBounds
    }
}