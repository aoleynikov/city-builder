using UnityEngine;

namespace ao.CityBuilder
{
    public struct OnBuildingPlaceRequest : IEvent
    {
        public Building Building { get; }
        public Vector2Int CellCoord { get; }

        public OnBuildingPlaceRequest(Building building, Vector2Int cellCoord)
        {
            Building = building;
            CellCoord = cellCoord;
        }
    }
}