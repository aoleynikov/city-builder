namespace ao.CityBuilder
{
    public struct OnFitStatus : IEvent
    {
        public FitStatus FitStatus { get; }

        public OnFitStatus(FitStatus fitStatus)
        {
            FitStatus = fitStatus;
        }
         
    }
}