using System;
using System.Collections.Generic;
using UnityEngine;

namespace ao.CityBuilder
{
    public class EventContainer<T> where T : IEvent
    {
        private event Action<T> handler;
        
        public EventContainer(Action<T> handler)
        {
            this.handler = handler;
        }

        public void AddListener(Action<T> handler)
        {
            this.handler += handler;
        }
        
        public void RemoveListener(Action<T> handler)
        {
            this.handler -= handler;
        }

        public void Invoke(T arg)
        {
            handler?.Invoke(arg);
        }
    }
   
    public class EventsBusSystem : InitializableSystemBase
    {
        private Dictionary<Type, object> events = new Dictionary<Type, object>();
        
        public void AddListener<T>(Action<T> action) where T : IEvent
        {
            var key = typeof(T);

            if (!events.ContainsKey(key))
            {
                events.Add(key, new EventContainer<T>(action));
            }
            else
            {
                ((EventContainer<T>)(events[typeof(T)])).AddListener(action);
            }
        }
        
        public void RemoveListener<T>(Action<T> action) where T : IEvent
        {
            var key = typeof(T);
            
            if (events.ContainsKey(key))
            {                
                ((EventContainer<T>)(events[typeof(T)])).RemoveListener(action);
            }
        }
        
        public void Fire<T>(T arg) where T : IEvent
        {
            if (events.ContainsKey(typeof(T)))
            {
                ((EventContainer<T>)(events[typeof(T)])).Invoke(arg);
            }
        }
    }
}