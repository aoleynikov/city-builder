namespace ao.CityBuilder
{
    /// <summary>
    /// Building item's chosen in the shop
    /// </summary>
    public struct OnBuildingTryToBuy : IEvent
    {
        public BuildingType BuildingType { get; }
    
        public OnBuildingTryToBuy(BuildingType buildingType)
        {
            BuildingType = buildingType;
        }
    }
}