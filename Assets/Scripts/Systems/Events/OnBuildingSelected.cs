namespace ao.CityBuilder
{
    public struct OnBuildingSelected : IEvent
    {
        public Building Building { get; }
        
        public OnBuildingSelected(Building building)
        {
            Building = building;
        }
    }
}