namespace ao.CityBuilder
{
    public struct OnModeSwitched : IEvent
    {
        public ModeType ModeType { get; }

        public OnModeSwitched(ModeType modeType)
        {
            ModeType = modeType;
        }
         
    }
}