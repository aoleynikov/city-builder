namespace ao.CityBuilder
{
    public struct OnBuildingPlaced : IEvent
    {
        public Building Building { get; }
         
        public OnBuildingPlaced(Building building)
        {
            Building = building;
        }
    }
}