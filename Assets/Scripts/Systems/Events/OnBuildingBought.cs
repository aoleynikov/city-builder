namespace ao.CityBuilder
{
    /// <summary>
    /// Building's bought
    /// </summary>
    public struct OnBuildingBought : IEvent
    {
        public Building Building { get; }
    
        public OnBuildingBought(Building building)
        {
            Building = building;
        }
    }
}