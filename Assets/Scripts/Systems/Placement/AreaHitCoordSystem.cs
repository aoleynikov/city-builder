using System;
using System.Collections.Generic;
using lab.framework;
using UnityEngine;

namespace ao.CityBuilder
{
    public class AreaHitCoordSystem : InitializableSystemBase, ITickable
    {
        [Inject] private IInputSystem inputSystem;

        private Vector3 worldSnappedCoord;
        public Vector3 WorldSnappedCoord => worldSnappedCoord;

        private Vector2 textureCoord;
        public Vector2 TextureCoord => textureCoord;

        private Vector2Int cellCoord;
        public Vector2Int CellCoord => cellCoord;

        private bool raycasted;
        public bool Raycasted => raycasted;
        
        private Renderer areaRenderer;
        public Renderer AreaRenderer => areaRenderer;

        public void Tick()
        {
            raycasted = false;
            
            Vector2 cursorPosition = inputSystem.GetCursorPosition();
            Ray ray = Camera.main.ScreenPointToRay(cursorPosition);

            if (Physics.Raycast(ray, out RaycastHit hit, 1000, Layers.LayerMask[LayerType.Area]))
            {
                if (!areaRenderer) areaRenderer = hit.transform.GetComponent<Renderer>();
                textureCoord = hit.textureCoord;

                TexHitToCellCoord(textureCoord);
                TexHitToWorldSnappedCoord(textureCoord);

                raycasted = true;
            }
        }

        private void TexHitToCellCoord(Vector2 pixelUV)
        {
            cellCoord.x =  Mathf.FloorToInt(InitSettings.PlacementSettings.AreaDimension - pixelUV.x * InitSettings.PlacementSettings.AreaDimension);
            cellCoord.y = Mathf.FloorToInt(pixelUV.y * InitSettings.PlacementSettings.AreaDimension);
        }

        private void TexHitToWorldSnappedCoord(Vector2 pixelUV)
        {
            float cellX = InitSettings.PlacementSettings.AreaDimension - pixelUV.x * InitSettings.PlacementSettings.AreaDimension;
            float cellY = 1 - pixelUV.y * InitSettings.PlacementSettings.AreaDimension;

            cellX =  Mathf.Floor(cellX);
            cellY = Mathf.Floor(cellY);
            
            worldSnappedCoord = new Vector3
            {
                x = cellX * InitSettings.PlacementSettings.TileSize,
                z = cellY * InitSettings.PlacementSettings.TileSize
            };
        }
    }
}