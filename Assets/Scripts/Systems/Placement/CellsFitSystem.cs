using lab.framework;
using UnityEngine;

namespace ao.CityBuilder
{
    public class CellsFitSystem : InitializableSystemBase, IDisposable
    {
        [Inject] private EventsBusSystem eventsBusSystem;

        private bool[,] cellsData;
        
        public override void Initialize(params object[] arg)
        {
            base.Initialize(arg);
           
            cellsData = new bool[InitSettings.PlacementSettings.AreaDimension, InitSettings.PlacementSettings.AreaDimension];
            
            eventsBusSystem.AddListener<OnBuildingPlaceRequest>(OnBuildingPlaceRequest);
        }
        
        public void Dispose()
        {
            eventsBusSystem.RemoveListener<OnBuildingPlaceRequest>(OnBuildingPlaceRequest);
        }

        private void OnBuildingPlaceRequest(OnBuildingPlaceRequest buildingPlaceRequest)
        {
            Vector2Int startCell = buildingPlaceRequest.CellCoord;
            Building building = buildingPlaceRequest.Building;
            
            Vector2Int bounds = new Vector2Int(startCell.x + building.BuildingSize.X, startCell.y + building.BuildingSize.Z);
            
            if (startCell.x < 0 || startCell.x >= cellsData.GetLength(0) ||
                startCell.y < 0 || startCell.y >= cellsData.GetLength(1) ||
                bounds.x >  cellsData.GetLength(0) || bounds.y > cellsData.GetLength(1))
            {
                eventsBusSystem.Fire(new OnFitStatus(FitStatus.OutOfBounds));
                return;
            }

            for (int y = startCell.y; y < bounds.y; y++)
            {
                for (int x = startCell.x; x < bounds.x; x++)
                {
                    if (cellsData[x, y])
                    {
                        eventsBusSystem.Fire(new OnFitStatus(FitStatus.Occupied));
                        return;
                    }
                }
            }

            PlaceBuilding(startCell, bounds);
            eventsBusSystem.Fire(new OnFitStatus(FitStatus.Fits));
        }

        private void PlaceBuilding(Vector2Int startCell, Vector2Int bounds)
        {
            for (int y = startCell.y; y < bounds.y; y++)
            {
                for (int x = startCell.x; x < bounds.x; x++)
                {
                    cellsData[x, y] = true;
                }
            }
        }
    }
}