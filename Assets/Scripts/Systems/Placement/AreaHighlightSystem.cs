using lab.framework;
using UnityEngine;

namespace ao.CityBuilder
{
    public class AreaHighlightSystem : InitializableSystemBase, ITickable
    {
        [Inject] private AreaHitCoordSystem areaHitCoordSystem;

        private Renderer areaRenderer;

        public void Tick()
        {
            if (areaHitCoordSystem.Raycasted)
            {
                if (!areaRenderer) areaRenderer = areaHitCoordSystem.AreaRenderer;
                HighlightCell(areaHitCoordSystem.TextureCoord);
            }
        }
        
        private void HighlightCell(Vector2 textureCoord)
        {
            areaRenderer.material.SetFloat("_SelectCell", 1);
            areaRenderer.material.SetFloat("_SelectedCellX", textureCoord.x * InitSettings.PlacementSettings.AreaDimension);
            areaRenderer.material.SetFloat("_SelectedCellY", textureCoord.y * InitSettings.PlacementSettings.AreaDimension);
        }
    }
}