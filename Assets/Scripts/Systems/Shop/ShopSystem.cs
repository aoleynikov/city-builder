using lab.framework;
using UnityEngine;

namespace ao.CityBuilder
{
    public class ShopSystem : InitializableSystemBase, IDisposable
    {
        [Inject] private EventsBusSystem eventsBusSystem;
        [Inject] private BuildingsFactorySystem buildingsFactorySystem;
        [Inject] private ResourcesUpdateSystem resourcesUpdateSystem;

        public override void Initialize(params object[] arg)
        {
            base.Initialize(arg);
            eventsBusSystem.AddListener<OnBuildingTryToBuy>(OnBuildingClickedInShop);
        }
        
        public void Dispose()
        {
            eventsBusSystem.RemoveListener<OnBuildingTryToBuy>(OnBuildingClickedInShop);
        }

        private void OnBuildingClickedInShop(OnBuildingTryToBuy onBuildingTryToBuy)
        {
            if (CanBuy(onBuildingTryToBuy.BuildingType))
            {
                Buy(onBuildingTryToBuy.BuildingType);
            }
            else
            {
                Debug.Log("Cannot buy " + onBuildingTryToBuy.BuildingType);
            }
        }

        private bool CanBuy(BuildingType buildingType)
        {
            Building building = buildingsFactorySystem.GetBuilding(buildingType);
            
            return (resourcesUpdateSystem.CurResources >= building.Price);
        }

        private void Buy(BuildingType buildingType)
        {
            Building building = buildingsFactorySystem.GetBuilding(buildingType);

            resourcesUpdateSystem.RemoveResources(building.Price);
            
            eventsBusSystem.Fire(new OnBuildingBought(building));
        }
    }
}