using System;
using lab.framework;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ao.CityBuilder
{
    public class MouseInputSystem: InitializableSystemBase, IInputSystem, ITickable
    {
        public event Action<Vector2> OnTap;

        public Vector2 GetCursorPosition()
        {
            return Input.mousePosition;
        }

        public void Tick()
        {
            bool pressed = Input.GetMouseButtonDown(0) && !IsOverUI();

            if (pressed)
            {
                OnTap?.Invoke(GetCursorPosition());
            }
        }

        private bool IsOverUI()
        {
            return EventSystem.current.IsPointerOverGameObject();
        }
    }
}