using System;
using UnityEngine;

namespace ao.CityBuilder
{
    public interface IInputSystem
    {
        event Action<Vector2> OnTap;
        Vector2 GetCursorPosition();
    }
}