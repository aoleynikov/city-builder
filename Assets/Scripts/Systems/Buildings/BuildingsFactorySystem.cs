using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

namespace ao.CityBuilder
{
    public class BuildingsFactorySystem : InitializableSystemBase
    {
        private Dictionary<BuildingType, Building> buildingByType = new Dictionary<BuildingType, Building>();
        
        public override void Initialize(params object[] arg)
        {
            base.Initialize(arg);
            
            InitSettings.BuildingsSettings.Buildings.ForEach((b) =>
            {
                buildingByType.Add(b.BuildingType, b);
            });
        }

        [CanBeNull]
        public Building GetBuilding(BuildingType buildingType)
        {
            if (buildingByType.ContainsKey(buildingType))
            {
                return buildingByType[buildingType];
            }
            else
            {
                Debug.LogError("No building of type presented: " + buildingType);
                return null;
            }
        }
    }
}