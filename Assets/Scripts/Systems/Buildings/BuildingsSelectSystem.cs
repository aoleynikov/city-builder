using lab.framework;
using UnityEngine;

namespace ao.CityBuilder
{
    public class BuildingsSelectSystem : InitializableSystemBase, IDisposable
    {
        [Inject] private IInputSystem inputSystem;
        [Inject] private EventsBusSystem eventsBusSystem;

        public override void Initialize(params object[] arg)
        {
            base.Initialize(arg);
            
            inputSystem.OnTap += InputSystemOnOnTap;
        }
        
        public void Dispose()
        {
            inputSystem.OnTap -= InputSystemOnOnTap;
        }

        private void InputSystemOnOnTap(Vector2 cursorPosition)
        {
            Ray ray = Camera.main.ScreenPointToRay(cursorPosition);

            if (Physics.Raycast(ray, out RaycastHit hit, 1000, Layers.LayerMask[LayerType.Buildings]))
            {
                Building building = hit.transform.gameObject.GetComponentInParent<Building>();

                if (building)
                {
                    eventsBusSystem.Fire(new OnBuildingSelected(building));
                }
            }
            else
            {
                eventsBusSystem.Fire(new OnBuildingDeselected());
            }
        }
    }
}