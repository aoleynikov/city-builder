using lab.framework;
using UnityEngine;

namespace ao.CityBuilder
{
    public class BuildingsPlacementSystem : InitializableSystemBase, ITickable, IDisposable
    {
        [Inject] private EventsBusSystem eventsBusSystem;
        [Inject] private AreaHitCoordSystem areaHitCoordSystem;
        [Inject] private IInputSystem inputSystem;

        private Vector3 buildingPrefabOffset = Vector3.zero;
        private readonly Vector3 placementOffset = new Vector3(0, 5, 0);

        private Building building;
        
        public override void Initialize(params object[] arg)
        {
            base.Initialize(arg);
            
            eventsBusSystem.AddListener<OnBuildingBought>(OnBuildingBought);
            eventsBusSystem.AddListener<OnFitStatus>(OnFitStatus);
            inputSystem.OnTap += InputSystemOnOnTap;
        }

        public void Dispose()
        {
            eventsBusSystem.RemoveListener<OnBuildingBought>(OnBuildingBought);
            eventsBusSystem.RemoveListener<OnFitStatus>(OnFitStatus);
            inputSystem.OnTap -= InputSystemOnOnTap;
        }

        public void Tick()
        {
            if (!building)
                return;

            building.transform.position = areaHitCoordSystem.WorldSnappedCoord + buildingPrefabOffset + placementOffset;
        }

        private void OnBuildingBought(OnBuildingBought onBuildingBought)
        {
            building = GameObject.Instantiate(onBuildingBought.Building);
            buildingPrefabOffset = building.transform.position;
        }
        
        private void InputSystemOnOnTap(Vector2 obj)
        {
            if (building)
            {
                eventsBusSystem.Fire(new OnBuildingPlaceRequest(building, areaHitCoordSystem.CellCoord));
            }
        }
        
        private void OnFitStatus(OnFitStatus fitStatus)
        {
            switch (fitStatus.FitStatus)
            {
                case FitStatus.Fits:
                    building.transform.position = areaHitCoordSystem.WorldSnappedCoord + buildingPrefabOffset;
                    
                    eventsBusSystem.Fire(new OnBuildingPlaced(building));
                    eventsBusSystem.Fire(new OnBuildingSelected(building));
                    
                    building = null;
                    break;
                
                case FitStatus.Occupied:
                case FitStatus.OutOfBounds:
                    break;
            }
        }
    }
}