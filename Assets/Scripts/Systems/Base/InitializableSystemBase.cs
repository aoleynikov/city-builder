using lab.framework;
using UnityEngine;

namespace ao.CityBuilder
{
    public class InitializableSystemBase: IInitializable
    {
        protected InitSettings InitSettings;
        
        public virtual void Initialize(params object[] arg)
        {
            InitSettings = arg[0] as InitSettings;
            if (InitSettings == null)
            {
                Debug.LogError("No InitSettings");
                return;
            }
        }
    }
}