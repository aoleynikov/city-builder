using lab.framework;
using UnityEngine;

namespace ao.CityBuilder
{
    public class ResourcesProductionSystem : InitializableSystemBase, IDisposable
    {
        [Inject] private EventsBusSystem eventsBusSystem;
        [Inject] private ResourcesUpdateSystem resourcesUpdateSystem;

        private ResourceProducerView selectedProducer;
        private ModeType curMode;

        public override void Initialize(params object[] arg)
        {
            base.Initialize(arg);
            
            eventsBusSystem.AddListener<OnBuildingPlaced>(OnBuildingPlaced);
            eventsBusSystem.AddListener<OnBuildingSelected>(OnBuildingSelected);
            eventsBusSystem.AddListener<OnBuildingDeselected>(OnBuildingDeselected);
            eventsBusSystem.AddListener<OnModeSwitched>(OnModeSwitched);
        }

        public void Dispose()
        {
            eventsBusSystem.RemoveListener<OnBuildingPlaced>(OnBuildingPlaced);
            eventsBusSystem.RemoveListener<OnBuildingSelected>(OnBuildingSelected);
            eventsBusSystem.RemoveListener<OnBuildingDeselected>(OnBuildingDeselected);
            eventsBusSystem.RemoveListener<OnModeSwitched>(OnModeSwitched);
        }

        private void OnBuildingPlaced(OnBuildingPlaced buildingPlaced)
        {
            ResourceProducer resourceProducer = buildingPlaced.Building.GetComponent<ResourceProducer>();
            
            if (!resourceProducer)
                return;
            
            resourceProducer.OnResourcesProduced += ResourceProducerOnOnResourcesProduced;
            AddView(resourceProducer);
        }
        
        private void OnBuildingSelected(OnBuildingSelected data)
        {
            if (curMode == ModeType.Build)
                return;
            
            ResourceProducerView resourceProducerView = data.Building.gameObject.GetComponentInChildren<ResourceProducerView>();
            
            if (!resourceProducerView)
                return;

            if (selectedProducer && selectedProducer != resourceProducerView)
            {
                selectedProducer.ShowData(false);
            }
            
            resourceProducerView.ShowData(true);
            selectedProducer = resourceProducerView;
        }
        
        private void OnBuildingDeselected(OnBuildingDeselected data)
        {
            if (selectedProducer)
            {
                selectedProducer.ShowData(false);
            }
        }
        
        private void OnModeSwitched(OnModeSwitched data)
        {
            curMode = data.ModeType;
            
            if (data.ModeType == ModeType.Build && selectedProducer)
            {
                selectedProducer.ShowData(false);
            }
        }

        private void ResourceProducerOnOnResourcesProduced(PlayerResources playerResources)
        {
            resourcesUpdateSystem.AddResources(playerResources);
        }

        private void AddView(ResourceProducer resourceProducer)
        {
            ResourceProducerView resourceProducerView = GameObject.Instantiate(InitSettings.ResourceSettings.ResourceProducerView, resourceProducer.transform, false);
            
            resourceProducerView.Setup(resourceProducer);
        }
    }
}