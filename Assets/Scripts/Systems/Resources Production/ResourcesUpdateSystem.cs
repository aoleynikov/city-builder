using System;
using lab.framework;
using UnityEngine;

namespace ao.CityBuilder
{
    public class ResourcesUpdateSystem : InitializableSystemBase, ILateInitializable
    {
        public event Action<PlayerResources> OnResourcesUpdate; 
        
        private PlayerResources curResources;
        public PlayerResources CurResources => curResources;

        public override void Initialize(params object[] arg)
        {
            base.Initialize(arg);
            
            curResources = InitSettings.ResourceSettings.InitialResources;
        }
        
        public void LateInitialize()
        {
            OnResourcesUpdate?.Invoke(curResources);
        }

        public void AddResources(PlayerResources playerResources)
        {
            curResources += playerResources;
            
            OnResourcesUpdate?.Invoke(curResources);
        }

        public void RemoveResources(PlayerResources playerResources)
        {
            curResources -= playerResources;
            
            OnResourcesUpdate?.Invoke(curResources);
        }
    }
}