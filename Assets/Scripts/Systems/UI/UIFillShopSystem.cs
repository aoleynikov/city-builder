using lab.framework;
using UnityEngine;

namespace ao.CityBuilder
{
    public class UIFillShopSystem : InitializableSystemBase, ILateInitializable
    {
        [Inject] private BuildingsFactorySystem buildingsFactorySystem;
        [Inject] private EventsBusSystem eventsBusSystem;
        
        private UIShop uiShop;
        
        public override void Initialize(params object[] arg)
        {
            base.Initialize(arg);

            UIModes uiModes = GameObject.FindObjectOfType<UIModes>();
            uiShop = uiModes.GetComponentInChildren<UIShop>(true);
        }

        public void LateInitialize()
        {
            FillWithItems();
        }

        private void FillWithItems()
        {
            InitSettings.BuildingsSettings.Buildings.ForEach((b) =>
            {
                Building building = buildingsFactorySystem.GetBuilding(b.BuildingType);
                if (building)
                {
                    UIBuildingItem item = GameObject.Instantiate(InitSettings.UISettings.UIBuildingItem, uiShop.transform, false);              
                    item.Init(building.DisplayedName, building.Price, building.BuildingType);
                    
                    item.OnBuildingItemClicked += OnOnBuildingItemClicked;
                }
            });
        }

        private void OnOnBuildingItemClicked(BuildingType buildingType)
        {
            eventsBusSystem.Fire(new OnBuildingTryToBuy(buildingType));
        }


    }
}