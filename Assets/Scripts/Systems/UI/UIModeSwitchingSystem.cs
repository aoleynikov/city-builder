using System.Collections.Generic;
using System.Linq;
using lab.framework;
using UnityEngine;

namespace ao.CityBuilder
{    
    public class UIModeSwitchingSystem : InitializableSystemBase, IDisposable
    {
        [Inject] private EventsBusSystem eventsBusSystem;
        
        private List<UIModeButtonClick> modeButtons = new List<UIModeButtonClick>();
        private UIModes uiModes;
        private ModeType curMode = ModeType.Regular;
        
        private Dictionary<ModeType, string> modeTrigger = new Dictionary<ModeType, string>
        {
            {ModeType.Regular, "Regular"},
            {ModeType.Build, "Build"},
        };

        public override void Initialize(params object[] arg)
        {
            base.Initialize(arg);

            modeButtons = GameObject.FindObjectsOfType<UIModeButtonClick>().ToList();
            
            modeButtons.ForEach((b) =>
            {
                b.OnModeButtonPressed += ChangeMode;
            });

            uiModes = GameObject.FindObjectOfType<UIModes>();
            
            eventsBusSystem.AddListener<OnBuildingBought>(OnBuidingBought);
        }
        
        public void Dispose()
        {
            eventsBusSystem.RemoveListener<OnBuildingBought>(OnBuidingBought);
        }

        private void OnBuidingBought(OnBuildingBought onBuildingBought)
        {
            ChangeMode(ModeType.Regular);
        }

        private void ChangeMode(ModeType mode)
        {
            if (modeTrigger.ContainsKey(mode) && curMode != mode)
            {
                curMode = mode;
                uiModes.SetMode(modeTrigger[mode]);
                
                eventsBusSystem.Fire(new OnModeSwitched(mode));
            }
        }
    }
}