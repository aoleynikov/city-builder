using lab.framework;
using UnityEngine;

namespace ao.CityBuilder
{
    public class UIResourcesSystem : InitializableSystemBase, IDisposable
    {
        [Inject] private ResourcesUpdateSystem resourcesUpdateSystem;

        private UIResources uiResources;

        public override void Initialize(params object[] arg)
        {
            base.Initialize(arg);

            uiResources = GameObject.FindObjectOfType<UIResources>();
            
            resourcesUpdateSystem.OnResourcesUpdate += OnResourcesUpdate;
        }
        
        public void Dispose()
        {
            resourcesUpdateSystem.OnResourcesUpdate -= OnResourcesUpdate;
        }

        private void OnResourcesUpdate(PlayerResources playerResources)
        {
            uiResources.SetResources(playerResources);
        }
    }
}