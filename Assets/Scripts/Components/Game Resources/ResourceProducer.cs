using System;
using UnityEngine;

namespace ao.CityBuilder
{
    public class ResourceProducer : MonoBehaviour
    {
        public event Action<PlayerResources> OnResourcesProduced;
        
        [SerializeField] private PlayerResources playerResources;
        [SerializeField] private float productionTime;
        [SerializeField] private bool autoStart;
        public bool AutoStart => autoStart;

        [SerializeField] private float progress;
        public float Progress => progress;
        
        private bool started = false;
        private float startTime = 0;

        private void Awake()
        {
            if (autoStart)
            {
                StartProduction();
            }
        }

        private void OnDestroy()
        {
            OnResourcesProduced = null;
        }

        public void StartProduction()
        {
            started = true;
            startTime = Time.time;
        }

        private void Update()
        {
            if (started)
            {
                float goneTime = Time.time - startTime;

                progress = goneTime / productionTime;

                if (goneTime >= productionTime)
                {
                    OnResourcesProduced?.Invoke(playerResources);
                    
                    started = false;
                    progress = 0;
                    
                    if (autoStart)
                    {
                        StartProduction();
                    }
                }
            }
        }
    }
}