using UnityEngine;
using UnityEngine.UI;

namespace ao.CityBuilder
{
    public class ResourceProducerView : MonoBehaviour
    {
        [SerializeField] private GameObject pivot;
        [SerializeField] private Image progressBar;
        [SerializeField] private Button productionButton;

        private ResourceProducer resourceProducer;

        public void Setup(ResourceProducer producer)
        {
            resourceProducer = producer;

            ShowData(false);
            ShowProductionButton(!resourceProducer.AutoStart);
            SetupProductionButton();
        }

        public void ShowData(bool show)
        {
            pivot.SetActive(show);
        }
        
        private void ShowProductionButton(bool show)
        {
            productionButton.transform.gameObject.SetActive(show);
        }

        private void SetupProductionButton()
        {
            productionButton.onClick.AddListener(delegate
            {
                ShowProductionButton(false);
                resourceProducer.StartProduction();
            });
            
            resourceProducer.OnResourcesProduced += delegate(PlayerResources resources)
            {
                ShowProductionButton(!resourceProducer.AutoStart);
            };
        }

        private void OnDestroy()
        {
            productionButton.onClick.RemoveAllListeners();
        }

        private void Update()
        {
            progressBar.fillAmount = resourceProducer.Progress;
        }
    }
}