using System;
using UnityEngine;

namespace ao.CityBuilder
{
    [Serializable]
    public struct PlayerResources
    {
        public int Gold;
        public int Steel;
        public int Wood;
        
        public string Log()
        {
            return "Gold = " + Gold + " Steel = " + Steel + " Wood " + Wood;
        }
        
        public static PlayerResources operator + (PlayerResources res1, PlayerResources res2)
        {
            return new PlayerResources
            {
                Gold = res1.Gold + res2.Gold,
                Steel = res1.Steel + res2.Steel,
                Wood = res1.Wood + res2.Wood
            };
        }
        
        public static PlayerResources operator - (PlayerResources res1, PlayerResources res2)
        {
            return new PlayerResources
            {
                Gold = res1.Gold - res2.Gold,
                Steel = res1.Steel - res2.Steel,
                Wood = res1.Wood - res2.Wood
            };
        }
        
        public static bool operator >= (PlayerResources res1, PlayerResources res2)
        {
            return res1.Gold >= res2.Gold && res1.Wood >= res2.Wood && res1.Steel >= res2.Steel;
        }

        public static bool operator <= (PlayerResources res1, PlayerResources res2)
        {
            return res1.Gold <= res2.Gold && res1.Wood <= res2.Wood && res1.Steel <= res2.Steel;
        }
    }
}