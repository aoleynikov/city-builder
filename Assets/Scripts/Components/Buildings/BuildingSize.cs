using System;

namespace ao.CityBuilder
{
    [Serializable]
    public struct BuildingSize
    {
        public int X;
        public int Z;
    }
}