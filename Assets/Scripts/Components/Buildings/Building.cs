using UnityEngine;

namespace ao.CityBuilder
{
    public class Building : MonoBehaviour
    {
        [SerializeField] private BuildingType buildingType;
        public BuildingType BuildingType => buildingType;

        [SerializeField] private string displayedName;
        public string DisplayedName => displayedName;

        [SerializeField] private PlayerResources price;
        public PlayerResources Price => price;

        [SerializeField] private BuildingSize buildingSize;
        public BuildingSize BuildingSize => buildingSize;
    }
}