using UnityEngine;
using UnityEngine.UI;

namespace ao.CityBuilder
{
    public class UIResources : MonoBehaviour
    {
        [SerializeField] private Text goldText;
        [SerializeField] private Text woodText;
        [SerializeField] private Text steelText;

        public void SetResources(PlayerResources playerResources)
        {
            SetFormattedAmount(goldText, playerResources.Gold);
            SetFormattedAmount(woodText, playerResources.Wood);
            SetFormattedAmount(steelText, playerResources.Steel);
        }

        private void SetFormattedAmount(Text text, int resource)
        {
            text.text = "[" + resource.ToString() + "]";
        }
    }
}