using UnityEngine;

namespace ao.CityBuilder
{
    [RequireComponent(typeof(Animator))]
    public class UIModes : MonoBehaviour
    {
        private Animator animator;

        private void Awake()
        {
            animator = GetComponent<Animator>();
        }

        public void SetMode(string mode)
        {          
            animator.SetTrigger(mode);
        }
    }
}