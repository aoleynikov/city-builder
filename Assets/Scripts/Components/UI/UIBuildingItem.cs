using System;
using UnityEngine;
using UnityEngine.UI;

namespace ao.CityBuilder
{
    [RequireComponent(typeof(Button))]
    public class UIBuildingItem : MonoBehaviour
    {
        public event Action<BuildingType> OnBuildingItemClicked;
        
        [SerializeField] private Text displayedName;
        [SerializeField] private Text priceGold;
        [SerializeField] private Text priceSteel;
        [SerializeField] private Text priceWood;

        private BuildingType buildingType;
        private Button button;

        private void Awake()
        {
            button = GetComponent<Button>();
            button.onClick.AddListener(delegate { OnBuildingItemClicked?.Invoke(buildingType); });
        }

        private void OnDestroy()
        {
            button.onClick.RemoveAllListeners();
        }

        public void Init(string buildingName, PlayerResources playerResources,  BuildingType type)
        {
            displayedName.text = buildingName;
            priceGold.text = playerResources.Gold.ToString();
            priceSteel.text = playerResources.Steel.ToString();
            priceWood.text = playerResources.Wood.ToString();
            buildingType = type;
        }
    }
}