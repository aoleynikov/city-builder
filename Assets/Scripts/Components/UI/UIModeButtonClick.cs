using System;
using UnityEngine;
using UnityEngine.UI;

namespace ao.CityBuilder
{
    /// <summary>
    /// Mode Button pressed action invoker
    /// </summary>
    [RequireComponent(typeof(Button))]
    public class UIModeButtonClick : MonoBehaviour
    {
        public event Action<ModeType> OnModeButtonPressed;
        
        [SerializeField] private ModeType modeType;
        private Button button;

        private void Awake()
        {
            button = GetComponent<Button>();
            button.onClick.AddListener(delegate
            {
                OnModeButtonPressed?.Invoke(modeType);
            });
        }

        private void OnDestroy()
        {
            button.onClick.RemoveAllListeners();
        }
    }
}