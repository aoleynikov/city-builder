using lab.framework;
using UnityEngine;

namespace ao.CityBuilder
{
    public class SceneBootstrap : BootstrapBase
    {
        [SerializeField] private InitSettings InitSettings;

        private EventsBusSystem eventsBusSystem;
        
        private IInputSystem inputSystem;

        private BuildingsFactorySystem buildingsFactorySystem;
        private BuildingsPlacementSystem buildingsPlacementSystem;
        private BuildingsSelectSystem buildingsSelectSystem;
        
        private UIModeSwitchingSystem uiModeSwitchingSystem;
        private UIFillShopSystem uiFillShopSystem;
        private UIResourcesSystem uiResourcesSystem;

        private ResourcesUpdateSystem resourcesUpdateSystem;
        private ResourcesProductionSystem resourcesProductionSystem;
        private ShopSystem shopSystem;

        private AreaHitCoordSystem areaHitCoordSystem;
        private AreaHighlightSystem areaHighlightSystem;
        private CellsFitSystem cellsFitSystem;

        public override void Awake()
        {
            base.Awake();
            
            // create async processor for coroutines usage
            InitSettings.AsyncProcessor = CreateAsyncProcessor("Async Processor");

            // global event bus
            eventsBusSystem = systemsContainer.Register(typeof(EventsBusSystem));
            
            // input (mouse input in case of PC gameplay)
            inputSystem = systemsContainer.Register<IInputSystem>(typeof(MouseInputSystem));

            // buildings
            buildingsFactorySystem = systemsContainer.Register(typeof(BuildingsFactorySystem));
            buildingsPlacementSystem = systemsContainer.Register(typeof(BuildingsPlacementSystem));
            buildingsSelectSystem = systemsContainer.Register(typeof(BuildingsSelectSystem));
            
            // ui
            uiModeSwitchingSystem = systemsContainer.Register(typeof(UIModeSwitchingSystem));
            uiFillShopSystem = systemsContainer.Register(typeof(UIFillShopSystem));
            uiResourcesSystem = systemsContainer.Register(typeof(UIResourcesSystem));

            // resources
            resourcesUpdateSystem = systemsContainer.Register(typeof(ResourcesUpdateSystem));
            resourcesProductionSystem = systemsContainer.Register(typeof(ResourcesProductionSystem));
            shopSystem = systemsContainer.Register(typeof(ShopSystem));

            // area
            areaHitCoordSystem = systemsContainer.Register(typeof(AreaHitCoordSystem));
            areaHighlightSystem = systemsContainer.Register(typeof(AreaHighlightSystem));
            cellsFitSystem = systemsContainer.Register(typeof(CellsFitSystem));
            
            // resolve dependencies & init systems
            systemsContainer.ResolveAll();
            systemsContainer.InitializeAll(InitSettings);
            systemsContainer.LateInitializeAll();
        }
    }
}